package com.task10;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import org.junit.Test;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;
import software.amazon.awssdk.regions.Region;

import java.util.*;

import static org.testng.AssertJUnit.assertEquals;
import static software.amazon.awssdk.enhanced.dynamodb.internal.AttributeValues.numberValue;
import static software.amazon.awssdk.enhanced.dynamodb.internal.AttributeValues.stringValue;

public class DBHelper {
    public APIGatewayProxyResponseEvent handleTables(APIGatewayProxyRequestEvent request) {


        if ("POST".equals(request.getHttpMethod())){
             int id = addToTable(request.getBody());
            return ApiHandler.okResponse("\"id\": "+id);
        } else {
            String data = getDataTables();
            return ApiHandler.okResponseWithJson(data);
         }

    }

    private String getDataTables() {

        String tableName = "cmtr-98b50f87-Tables-test";

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        try {
            ScanRequest scanRequest = ScanRequest.builder()
                    .tableName(tableName)
                    .build();

            ScanResponse response = ddb.scan(scanRequest);
            List<Map<String,Object>> items = new ArrayList<>();

            for (Map<String, AttributeValue> item : response.items()) {
                Map<String,Object> map = new HashMap<>();
                map.put("id",Double.parseDouble(item.get("id").s()));
                map.put("number",Integer.parseInt(item.get("number").n()));
                map.put("places",Integer.parseInt(item.get("places").n()));
                map.put("isVip",Boolean.parseBoolean(item.get("isVip").s()));
                map.put("minOrder",Integer.parseInt(item.get("minOrder").n()));
                items.add(map);
            }
            Gson gson = new Gson();
            Map<String,List> map = new HashMap<>();
            map.put("tables",items);

            return gson.toJson(map);

        } catch (DynamoDbException e) {
            e.printStackTrace();
            return e.getMessage();

        }
    }

    private int addToTable(String body) {

        Gson gson = new Gson();

        HashMap r = gson.fromJson(body, HashMap.class);


        HashMap<String, AttributeValue> itemValues = new HashMap<>();

        itemValues.put("id",AttributeValue.builder().s(String.valueOf(r.get("id"))).build());
        itemValues.put("number",AttributeValue.builder().n(String.valueOf(r.get("number"))).build());
        itemValues.put("places",AttributeValue.builder().n(String.valueOf(r.get("places"))).build());
        itemValues.put("isVip",AttributeValue.builder().s(String.valueOf(r.get("isVip"))).build());
        itemValues.put("minOrder",AttributeValue.builder().n(String.valueOf(r.get("minOrder"))).build());

        System.out.println(itemValues);

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        String tableName = "cmtr-98b50f87-Tables-test";

        try {
            System.out.println("size: "+itemValues.entrySet().size());

            PutItemRequest request = PutItemRequest.builder()
                    .tableName(tableName)
                    .item(itemValues)
                    .build();

            PutItemResponse response = ddb.putItem(request);
            System.out.println(tableName + " was successfully updated. The request id is "
                    + response.responseMetadata().requestId());

        } catch (ResourceNotFoundException e) {
            System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        Double d = (Double)r.get("id");
        return d.intValue();
    }

    public APIGatewayProxyResponseEvent handleTableWithId(APIGatewayProxyRequestEvent request,String id) {
        String tableName = "cmtr-98b50f87-Tables-test";

        id = id+".0";

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        HashMap<String, AttributeValue> keyToGet = new HashMap<>();
        keyToGet.put("id", AttributeValue.builder()
                .s(id)
                .build());

        GetItemRequest req = GetItemRequest.builder()
                .key(keyToGet)
                .tableName(tableName)
                .build();

        try {
            // If there is no matching item, GetItem does not return any data.
            Map<String, AttributeValue> returnedItem = ddb.getItem(req).item();
            if (returnedItem.isEmpty()) {
                return ApiHandler.errorResponse("No item found with the key " + id);
            } else {
                HashMap<String,Object> map = new HashMap<>();

                Double d = Double.parseDouble(returnedItem.get("id").s());
                map.put("id",d.intValue());
                map.put("number",Integer.parseInt(returnedItem.get("number").n()));
                map.put("places",Integer.parseInt(returnedItem.get("places").n()));
                map.put("isVip",Boolean.parseBoolean(returnedItem.get("isVip").s()));
                map.put("minOrder",Integer.parseInt(returnedItem.get("minOrder").n()));

                Gson gson = new Gson();
                return ApiHandler.okResponseWithJson(gson.toJson(map));
            }

        } catch (DynamoDbException e) {
            return ApiHandler.errorResponse(e.getMessage());
        }

    }

    public APIGatewayProxyResponseEvent handleReservations(APIGatewayProxyRequestEvent request) {


        if ("POST".equals(request.getHttpMethod())){
            String uuid = addToReservation(request.getBody());
            if (uuid.equals("no data")){
                return ApiHandler.errorResponse("wrong data");
            }
            return ApiHandler.okResponse("\"reservationId\": \""+uuid+"\"");
        } else {
            String data = getDataReservations();
            return ApiHandler.okResponseWithJson(data);
        }

    }

    private String addToReservation(String body) {
        Gson gson = new Gson();

        HashMap r = gson.fromJson(body, HashMap.class);

        int tableNumber = new Double(String.valueOf(r.get("tableNumber"))).intValue();

        HashMap<String, AttributeValue> itemValues = new HashMap<>();

        if (!checkCanAddReservation(
                String.valueOf(r.get("slotTimeStart")),
                String.valueOf(r.get("slotTimeEnd")),
                String.valueOf(r.get("date")),
                String.valueOf(tableNumber))){
            return "no data";
        }
        System.out.println("continue...");

        UUID uuid = UUID.randomUUID();

        itemValues.put("id",AttributeValue.builder().s(String.valueOf(uuid)).build());
        itemValues.put("tableNumber",AttributeValue.builder().n(String.valueOf(r.get("tableNumber"))).build());
        itemValues.put("clientName",AttributeValue.builder().s(String.valueOf(r.get("clientName"))).build());
        itemValues.put("phoneNumber",AttributeValue.builder().s(String.valueOf(r.get("phoneNumber"))).build());
        itemValues.put("date",AttributeValue.builder().s(String.valueOf(r.get("date"))).build());
        itemValues.put("slotTimeStart",AttributeValue.builder().s(String.valueOf(r.get("slotTimeStart"))).build());
        itemValues.put("slotTimeEnd",AttributeValue.builder().s(String.valueOf(r.get("slotTimeEnd"))).build());

        System.out.println(itemValues);

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        String tableName = "cmtr-98b50f87-Reservations-test";

        try {
            System.out.println("size: "+itemValues.entrySet().size());

            PutItemRequest request = PutItemRequest.builder()
                    .tableName(tableName)
                    .item(itemValues)
                    .build();

            PutItemResponse response = ddb.putItem(request);
            System.out.println(tableName + " was successfully updated. The request id is "
                    + response.responseMetadata().requestId());

        } catch (ResourceNotFoundException e) {
            System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

       // Double d = (Double)r.get("id");
        return uuid.toString();
    }

    private String getDataReservations() {
        String tableName = "cmtr-98b50f87-Reservations-test";

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        try {
            ScanRequest scanRequest = ScanRequest.builder()
                    .tableName(tableName)
                    .build();

            ScanResponse response = ddb.scan(scanRequest);
            List<Map<String,Object>> items = new ArrayList<>();

            for (Map<String, AttributeValue> item : response.items()) {
                Map<String,Object> map = new HashMap<>();
                map.put("tableNumber",Integer.parseInt(item.get("tableNumber").n()));
                map.put("clientName",item.get("clientName").s());
                map.put("phoneNumber",item.get("phoneNumber").s());
                map.put("date",item.get("date").s());
                map.put("slotTimeStart",item.get("slotTimeStart").s());
                map.put("slotTimeEnd",item.get("slotTimeEnd").s());
                items.add(map);


            }
            Gson gson = new Gson();
            Map<String,List> map = new HashMap<>();
            map.put("reservations",items);

            return gson.toJson(map);

        } catch (DynamoDbException e) {
            e.printStackTrace();
            return e.getMessage();

        }
    }

    @Test
    public void test(){

        assertEquals(false,checkCanAddReservation("11:00","13:01", "2024-07-05","27"));
        assertEquals(true,checkCanAddReservation("11:00","13:00", "2024-07-05","27"));
        assertEquals(false,checkCanAddReservation("13:00","14:00", "2024-07-05","27"));
        assertEquals(false,checkCanAddReservation("12:50","13:50", "2024-07-05","27"));
        assertEquals(true,checkCanAddReservation("11:00","13:01", "2024-07-06","27"));
        assertEquals(false,checkCanAddReservation("11:00","17:01", "2024-07-05","27"));


    }

    private boolean checkCanAddReservation(String start,String end, String day, String tableNumber) {

        String tableName = "cmtr-98b50f87-Tables-test";

        Region region = Region.EU_CENTRAL_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        boolean tableExists = false;

        try {
            ScanRequest scanRequest = ScanRequest.builder()
                    .tableName(tableName)
                    .build();

            ScanResponse response = ddb.scan(scanRequest);
            List<Map<String, Object>> items = new ArrayList<>();

            for (Map<String, AttributeValue> item : response.items()) {
                System.out.println(tableNumber);
                System.out.println(item.get("number").n());
                System.out.println(tableNumber.equals(item.get("number").n()));
                if (tableNumber.equals(item.get("number").n())) {
                    tableExists = true;
                    break;
                }
            }
        }catch (Exception e){
            System.out.println("error: "+e.getMessage());
        }

        boolean timeOverlaps = false;

        tableName = "cmtr-98b50f87-Reservations-test";

        try {
            ScanRequest scanRequest = ScanRequest.builder()
                    .tableName(tableName)
                    .build();

            ScanResponse response = ddb.scan(scanRequest);
            List<Map<String, Object>> items = new ArrayList<>();

            for (Map<String, AttributeValue> item : response.items()) {
                if (day.equals(item.get("date").s()) && tableNumber.equals(item.get("tableNumber").n())) {

                    if (end.compareTo(item.get("slotTimeEnd").s()) >= 0 && start.compareTo(item.get("slotTimeStart").s()) <= 0) {

                        timeOverlaps = true;
                        break;
                    }

                    if ((end.compareTo(item.get("slotTimeStart").s()) > 0 && start.compareTo(item.get("slotTimeStart").s()) < 0) ||
                            (start.compareTo(item.get("slotTimeEnd").s()) < 0) && end.compareTo(item.get("slotTimeEnd").s()) > 0) {

                        timeOverlaps = true;
                        break;
                    }

                }
            }
        }catch (Exception e){
            System.out.println("error2: "+e.getMessage());
        }


        return tableExists && !timeOverlaps;


    }

    public static int transaction(String tableName, String partitionKeyName, String partitionKeyVal,
                                 String partitionAlias) {

        // String tableName = "cmtr-98b50f87-Reservations-test";

        Region region = Region.US_EAST_1;
        DynamoDbClient ddb = DynamoDbClient.builder()
                .region(region)
                .build();

        // Set up an alias for the partition key name in case it's a reserved word.
        HashMap<String, String> attrNameAlias = new HashMap<String, String>();
        attrNameAlias.put(partitionAlias, partitionKeyName);

        // Set up mapping of the partition name with the value.
        HashMap<String, AttributeValue> attrValues = new HashMap<>();
        attrValues.put(":" + partitionKeyName, AttributeValue.builder()
                .s(partitionKeyVal)
                .build());


        Map<String, AttributeValue> expressionValues = Map.of(
                ":id", numberValue(2));

        Map<String, String> expressionNames = Map.of(
            //    ":min_value", "2");
                "#id", "id");

        ConditionCheck check = ConditionCheck.builder().tableName("cmtr-98b50f87-Tables-test").conditionExpression("attribute_exists(id)")
                .key(Map.of(":id",stringValue("2")))
               // .expressionAttributeValues(expressionValues)
                //.expressionAttributeNames(Map.of("#pk","id"))
                .build();

        Put put = Put.builder().tableName("cmtr-98b50f87-Reservations-test").item(expressionValues)
                .build(); // condition exp to ensure not overlapping.

        TransactWriteItem wi =  TransactWriteItem.builder().conditionCheck(check)
                //.put(put)
                .build();

        TransactWriteItemsRequest request = TransactWriteItemsRequest.builder()
                .transactItems(wi)
                .build();




//        ScanEnhancedRequest request = ScanEnhancedRequest.builder()
//                .consistentRead(true).
//                // 1. the 'attributesToProject()' method allows you to specify which values you want returned.
//                .attributesToProject(partitionKeyName)
//                // 2. Filter expression limits the items returned that match the provided criteria.
//                .filterExpression(Expression.builder()
//                        //.expression("price >= :min_value AND price <= :max_value")
//                        .expression(partitionKeyName +" = :min_value")
//                        .expressionValues(expressionValues)
//                        .build())
//                .build();

        TransactWriteItemsResponse response = null;

        try {
            response = ddb.transactWriteItems(request);
            System.out.println(response.toString());


        } catch (DynamoDbException e) {
            System.out.println("trans: "+e.getMessage());
           // e.printStackTrace();
            return -1;
        }
        return 0;
    }


}
