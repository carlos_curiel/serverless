package com.task10;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import com.google.gson.Gson;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import com.syndicate.deployment.annotations.environment.EnvironmentVariables;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.RetentionSetting;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role",
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)
@EnvironmentVariables(value = {
		@EnvironmentVariable(key = "region", value = "${region}"),
		@EnvironmentVariable(key = "booking_userpool", value = "${booking_userpool}")
})
public class ApiHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	private DBHelper dbHelper = new DBHelper();

	public static APIGatewayProxyResponseEvent okResponseWithJson(String data) {
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setBody(data);
		response.setStatusCode(200);

		return response;
	}

	public APIGatewayProxyResponseEvent  handleRequest(APIGatewayProxyRequestEvent request, Context context) {
		String booking_userpool = System.getenv("booking_userpool");

		System.out.println(" lambda request: "+request);
		System.out.println(" lambda request: "+request.toString());

		String path = request.getPath();
		String method = request.getHttpMethod();

		System.out.println("Hello from lambda: "+path);

		CognitoIdentityProviderClient identityProviderClient = CognitoIdentityProviderClient.builder()
				.region(Region.EU_CENTRAL_1)
				.build();
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

		ListUserPoolsRequest req = ListUserPoolsRequest.builder().build();

		String id = identityProviderClient.listUserPools(req).userPools().stream().filter(e->e.name().equals(booking_userpool)).collect(Collectors.toList()).get(0).id();

		ListUserPoolClientsRequest req2 = ListUserPoolClientsRequest.builder().userPoolId(id).build();

		String clientId = identityProviderClient.listUserPoolClients(req2).userPoolClients().get(0).clientId();

		String body = request.getBody();

		Gson gson = new Gson();
		HashMap<String, String> hm = gson.fromJson(body, HashMap.class);


		if (path.equals("/signup")) {
			System.out.println("/signup");


			//signUp(identityProviderClient,"7qdv6hkrupfa0gdsiq2qvhf8c3","key",hm.get("email"),hm.get("password"),hm.get("email"));
			int status = signUp(identityProviderClient, id, hm.get("email"), hm.get("password"), hm.get("email")).getStatusCode();


			if (status != 200){
				return errorResponse("Error");
			} else{
				confirm(identityProviderClient, id, hm.get("email"), hm.get("password"), clientId);
				return  okResponse();
			}

		} else if (path.equals("/signin")) {

			return signIn(identityProviderClient, id, hm.get("email"), hm.get("password"),clientId);

		} else if (path.equals("/tables")) {
			System.out.println("headers: " + request.getHeaders().toString());
			if (request.getPathParameters()!=null) System.out.println("path params: " + request.getPathParameters().toString());
			if (request.getQueryStringParameters()!=null) System.out.println("query string params: " + request.getQueryStringParameters().toString());
			System.out.println("method: " + method);
			System.out.println("request: " + request.toString());
			System.out.println("body: " + body);
			if (request.getStageVariables()!=null)
				System.out.println("getStageVariables: " + request.getStageVariables().toString());

			return dbHelper.handleTables(request);
		} else if (path.startsWith("/tables/")) {
			if (request.getPathParameters()!=null) System.out.println("path params: " + request.getPathParameters().toString());
			if (request.getQueryStringParameters()!=null) System.out.println("query string params: " + request.getQueryStringParameters().toString());
			//String idTable = path.substring(path.lastIndexOf('/'));
			String idTable = request.getPathParameters().get("tableId");
			System.out.println("idTable: " + idTable);
			System.out.println("headers: " + request.getHeaders().toString());
			return dbHelper.handleTableWithId(request,idTable);


		} else if (path.startsWith("/reservations")) {

			return dbHelper.handleReservations(request);


		} else {
			return errorResponse("No data");
		}

	}

	private APIGatewayProxyResponseEvent signIn(CognitoIdentityProviderClient identityProviderClient, String id, String email, String password, String clientId) {

		if (email.isBlank() || password.isBlank()) {
			return errorResponse("Invalid data");

		}


		try
		{
			Map<String,String> authParams = new HashMap<String,String>();
			authParams.put("USERNAME", email);
			authParams.put("PASSWORD", password);

			AdminInitiateAuthRequest authRequest = AdminInitiateAuthRequest.builder()
					.authFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
					.authParameters(authParams)
					.clientId(clientId)
					.userPoolId(id).build();





			AdminInitiateAuthResponse authResponse = identityProviderClient.adminInitiateAuth(authRequest);

			System.out.println("signing in: "+email +" : "+password);

			System.out.println("auth resp: "+authResponse);

			System.out.println("auth resp: "+authResponse.authenticationResult());



			String token = authResponse.authenticationResult().accessToken();
			System.out.println("token: "+ token);



			return okResponse("\"accessToken\": \""+token+"\"");
/*
			if (ChallengeNameType.NEW_PASSWORD_REQUIRED == authResponse.challengeName()) {
				System.out.println("{} attempted to sign in with temporary password");
				return okResponse("\"accessToken\": "+token);
			}
			else {
				throw new RuntimeException("unexpected challenge on signin: " + authResponse.challengeName());
			}*/
		}
		catch (UserNotFoundException ex)
		{

			return errorResponse("No such user");
		}
		catch (NotAuthorizedException ex)
		{

			return errorResponse("Not authorized");
		}
		catch (TooManyRequestsException ex)
		{
			return errorResponse("too many requests");
		} catch (Exception e){
			System.out.println(e.getMessage());
			return errorResponse("General error");
		}
	}

	public static APIGatewayProxyResponseEvent okResponse() {
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setBody("{\"All good\"}");
		response.setStatusCode(200);

		return response;
	}

	public static APIGatewayProxyResponseEvent okResponse(String msg) {
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setBody("{"+msg+"}");
		response.setStatusCode(200);

		return response;
	}

	public static  APIGatewayProxyResponseEvent errorResponse(String error) {
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

		response.setBody("{\""+error+"\"}");
		response.setStatusCode(400);

		return response;
	}

	public void signUp(CognitoIdentityProviderClient identityProviderClient,
							  String clientId,
							  String secretKey,
							  String userName,
							  String password,
							  String email) {

		AttributeType attributeType = AttributeType.builder()
				.name("email")
				.value(email)
				.build();

		List<AttributeType> attrs = new ArrayList<>();
		attrs.add(attributeType);
		System.out.println("signUp...");
		try {
			String secretVal = calculateSecretHash(clientId, secretKey, userName);
			System.out.println("signUp 2...");
			SignUpRequest signUpRequest = SignUpRequest.builder()
					.userAttributes(attrs)
					.username(userName)
					.clientId(clientId)
					.password(password)
					.secretHash(secretVal)
					.build();
			System.out.println("signUp 3...");
			SignUpResponse res = identityProviderClient.signUp(signUpRequest);
			System.out.println("User has been signed up: "+res.toString());

		} catch (Exception e){
			errorResponse(e.getMessage());
		}
	}

	public String calculateSecretHash(String userPoolClientId, String userPoolClientSecret, String userName)
			throws NoSuchAlgorithmException, InvalidKeyException {
		final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

		SecretKeySpec signingKey = new SecretKeySpec(
				userPoolClientSecret.getBytes(StandardCharsets.UTF_8),
				HMAC_SHA256_ALGORITHM);

		Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(signingKey);
		mac.update(userName.getBytes(StandardCharsets.UTF_8));
		byte[] rawHmac = mac.doFinal(userPoolClientId.getBytes(StandardCharsets.UTF_8));
		return java.util.Base64.getEncoder().encodeToString(rawHmac);
	}

	public static void signUp1(CognitoIdentityProviderClient identityProviderClient, String clientId, String userName,
							  String password, String email) {
		AttributeType userAttrs = AttributeType.builder()
				.name("email")
				.value(email)
				.build();

		List<AttributeType> userAttrsList = new ArrayList<>();
		userAttrsList.add(userAttrs);
		try {
			SignUpRequest signUpRequest = SignUpRequest.builder()
					.userAttributes(userAttrsList)
					.username(userName)
					.clientId(clientId)
					.password(password)
					.build();

			identityProviderClient.signUp(signUpRequest);
			System.out.println("User has been signed up ");

		} catch (CognitoIdentityProviderException e) {
			System.out.println("signUp: "+ e.awsErrorDetails().errorMessage());
		}
	}

	public APIGatewayProxyResponseEvent signUp(CognitoIdentityProviderClient identityProviderClient, String id, String userName,
					   String password, String email){
		try
		{

			AttributeType userAttrs = AttributeType.builder()
					.name("email")
					.value(email)
					.build();

			AdminCreateUserRequest cognitoRequest = AdminCreateUserRequest.builder()
					.userPoolId(id).userAttributes(userAttrs).messageAction("SUPPRESS")
					.username(email).temporaryPassword(password)
					.desiredDeliveryMediums(DeliveryMediumType.EMAIL)
					.forceAliasCreation(Boolean.FALSE).build();
			

			identityProviderClient.adminCreateUser(cognitoRequest);

			System.out.println("registered: "+userName +" : "+password);

			

		}
		catch (UsernameExistsException ex)
		{
			System.out.println("user already exists: " +email);
		}catch (Exception e){
			return errorResponse(e.getMessage());
		}

		return okResponse();
	}

	private void confirm(CognitoIdentityProviderClient identityProviderClient, String id, String email, String password, String clientId) {

		Map<String, String> initialParams = new HashMap<String, String>();
		initialParams.put("USERNAME", email);
		initialParams.put("PASSWORD", password);

		AdminInitiateAuthRequest initialRequest = AdminInitiateAuthRequest.builder()
				.authFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
				.authParameters(initialParams)
				.clientId(clientId)
				.userPoolId(id).build();

		AdminInitiateAuthResponse initialResponse = identityProviderClient.adminInitiateAuth(initialRequest);
		if (!ChallengeNameType.NEW_PASSWORD_REQUIRED.name().equals(initialResponse.challengeNameAsString())) {
			throw new RuntimeException("unexpected challenge: " + initialResponse.challengeNameAsString());
		}

		Map<String, String> challengeResponses = new HashMap<String, String>();
		challengeResponses.put("USERNAME", email);
		challengeResponses.put("PASSWORD", password);
		challengeResponses.put("NEW_PASSWORD", password);

		AdminRespondToAuthChallengeRequest finalRequest = AdminRespondToAuthChallengeRequest.builder()
				.challengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
				.challengeResponses(challengeResponses)
				.clientId(clientId)
				.userPoolId(id)
				.session(initialResponse.session()).build();

		AdminRespondToAuthChallengeResponse challengeResponse = identityProviderClient.adminRespondToAuthChallenge(finalRequest);

		System.out.println(challengeResponse);
		System.out.println(challengeResponse.authenticationResult());

	}
}
