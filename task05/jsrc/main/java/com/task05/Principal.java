package com.task05;

import java.util.Map;

public class Principal {

    private String principalId;
    private Map<String,String> content;

    public String getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(String principalId) {
        this.principalId = principalId;
    }

    public Map<String,String> getContent() {
        return content;
    }

    public void setContent(Map<String,String> content) {
        this.content = content;
    }


}


