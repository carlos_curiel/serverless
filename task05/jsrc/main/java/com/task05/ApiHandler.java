package com.task05;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import com.syndicate.deployment.annotations.environment.EnvironmentVariables;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.RetentionSetting;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import software.amazon.awssdk.thirdparty.jackson.core.JsonProcessingException;

@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role",
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)
@EnvironmentVariables(value = {
		@EnvironmentVariable(key = "region", value = "${region}"),
		@EnvironmentVariable(key = "prefix", value = "${resources_prefix}"),
		@EnvironmentVariable(key = "suffix", value = "${resources_suffix}")
})
public class ApiHandler implements RequestHandler<Object, Map<String, Object>> {

	public Map<String, Object> handleRequest(Object request, Context context) {

		Region region = Region.of(System.getenv("region"));
		DynamoDbClient ddb = DynamoDbClient.builder()
				.region(region)
				.build();
		System.out.println("Hello from lambda: "+request);

        String tableName = "cmtr-98b50f87-"+"Events"+"-test";
		System.out.println(tableName);

		Gson gson = new Gson();

		Principal p = gson.fromJson(request.toString(), Principal.class);


		String id = "id";
		UUID uuid = UUID.randomUUID();
		String idVal = uuid.toString();
		String principalId = "principalId";
		String principalIdVal = p.getPrincipalId();
		String createdAt = "createdAt";
		String nameVal = (String) p.getContent().get("name");
		String surnameVal = (String) p.getContent().get("surname");

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset

		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());

		putItemInTable(ddb, tableName, id, idVal, principalId, principalIdVal, createdAt, nowAsISO, p);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("statusCode", 201);
		resultMap.put("body", "Created: "+id);
		return resultMap;
	}

	public static void putItemInTable(DynamoDbClient ddb,
									  String tableName,
									  String id,
									  String idVal,
									  String principalId,
									  String principalIdValue,
									  String createdAt,
									  String createdAtVal,
									  Principal p) {

		HashMap<String, AttributeValue> itemValues = new HashMap<>();
		itemValues.put(id, AttributeValue.builder().s(idVal).build());
		itemValues.put(principalId, AttributeValue.builder().n(principalIdValue).build());
		itemValues.put(createdAt, AttributeValue.builder().s(createdAtVal).build());
		Map<String, AttributeValue> map = new HashMap<>();
		for (Object e : p.getContent().entrySet()){
			map.put(((Map.Entry<String,String>)e).getKey(),AttributeValue.builder().s(((Map.Entry<String,String>)e).getValue()).build());

		}

		p.getContent().forEach((key, value) -> map.put(key, AttributeValue.builder().s(value).build()));
		//p.getContent().entrySet().stream().forEach(e->map.put(e.getKey(),AttributeValue.builder().s(e.getValue()).build()));




				//	.entrySet().stream().forEach(e->map.put(e.getKey(),e.getValue()));
				//	Map.entry().get

		itemValues.put("body", AttributeValue.builder().m(map).build());


		PutItemRequest request = PutItemRequest.builder()
				.tableName(tableName)
				.item(itemValues)
				.build();

		try {
			PutItemResponse response = ddb.putItem(request);
			System.out.println(tableName + " was successfully updated. The request id is "
					+ response.responseMetadata().requestId());

		} catch (ResourceNotFoundException e) {
			System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
			System.err.println("Be sure that it exists and that you've typed its name correctly!");
			System.exit(1);
		} catch (DynamoDbException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	@Test
	public void test(){
		Gson gson = new Gson();

		Principal p = gson.fromJson("{\"principalId\": 1, \"content\": {\"name\": \"Jacinto\", \"surname\": \"Rubirosa\"} }", Principal.class);


		System.out.println(p.getContent().get("name"));

		String request ="{\"rateName\": \"My Special Rate\",\"adjustments\":[{\"adjustmentType\": \"LOAN_AMOUNT_GREATER_THAN_550K\",\"rate\": 0.75},{\"adjustmentType\": \"AMORTIZATION_TERM_LESS_THAN_30_YEARS\",\"rate\": -0.2}],\"errorTypes\": [],\"premiumTaxs\": [],\"renewalPremiums\": [],\"totalInitialRate\": 1.95,\"optimumPricing\": false,\"miPricingVO\": null,\"rateCardId\": \"BALS_NR\",\"ratingInfoBaseRate\": 1.}";


	}

	@Test
	public void  test2(){

		DynamoDbClient ddb = DynamoDbClient.builder()
				.region(Region.of("eu-central-1"))
				.build();

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());

		System.out.println(nowAsISO);

	}


	}




