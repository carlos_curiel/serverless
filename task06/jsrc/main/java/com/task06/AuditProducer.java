package com.task06;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.StreamsEventResponse;
//import com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;
import com.amazonaws.services.lambda.runtime.events.models.dynamodb.StreamRecord;
import com.syndicate.deployment.annotations.events.DynamoDbTriggerEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.RetentionSetting;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@LambdaHandler(lambdaName = "audit_producer",
	roleName = "audit_producer-role",
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)

@DynamoDbTriggerEventSource(targetTable="Configuration",batchSize = 10)

public class AuditProducer implements RequestHandler<DynamodbEvent, Serializable> {

	public StreamsEventResponse handleRequest(DynamodbEvent input, Context context) {
		System.out.println("Hello from lambda: "+input.getRecords().size());

		List<StreamsEventResponse.BatchItemFailure> batchItemFailures = new ArrayList<>();
		String curRecordSequenceNumber = "";

		for (DynamodbEvent.DynamodbStreamRecord dynamodbStreamRecord : input.getRecords()) {
			try {
				//Process your record
				StreamRecord dynamodbRecord = dynamodbStreamRecord.getDynamodb();
				curRecordSequenceNumber = dynamodbRecord.getSequenceNumber();
				processRecord(dynamodbRecord.getOldImage(),dynamodbRecord.getNewImage());
				System.out.print(dynamodbRecord.getOldImage()+"  *  ");
				System.out.println(dynamodbRecord.getNewImage());

			} catch (Exception e) {
                /* Since we are working with streams, we can return the failed item immediately.
                   Lambda will immediately begin to retry processing from this failed item onwards. */
				e.printStackTrace();
				batchItemFailures.add(new StreamsEventResponse.BatchItemFailure(curRecordSequenceNumber));
				return new StreamsEventResponse(batchItemFailures);
			}
		}

		return new StreamsEventResponse();

	}

	private void processRecord(Map<String, com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue> oldImage,
							   Map<String, com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue> newImage) {

		HashMap<String, AttributeValue> itemValues = new HashMap<>();

		itemValues.put("id", AttributeValue.builder().s(String.valueOf(UUID.randomUUID())).build());
		itemValues.put("itemKey", getAttValue(newImage.get("key")));
		itemValues.put("modificationTime",getTime());

		if (oldImage==null){ // New record created
			HashMap<String, AttributeValue> newValues = new HashMap<>();
			newImage.entrySet().forEach(e->newValues.put(e.getKey(),getAttValue(e.getValue())));
			newImage.entrySet().forEach(e->newValues.put(e.getKey(),getAttValue(e.getValue())));
			itemValues.put("newValue",AttributeValue.builder().m(newValues).build());

		} else { // updated record

			List<Map.Entry<String,com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue>> modified = oldImage.entrySet().stream().filter(Predicate.not(e->newImage.get(e.getKey()).equals(e.getValue()))).collect(Collectors.toList());

			if (modified.size() > 0) {
				itemValues.put("updatedAttribute", AttributeValue.builder().s(modified.get(0).getKey()).build());
				itemValues.put("oldValue", getAttValue(oldImage.get(modified.get(0).getKey())));
				itemValues.put("newValue", getAttValue(newImage.get(modified.get(0).getKey())));
			}

		}
		System.out.println("items: "+itemValues.size());
		pushToDB(itemValues);
	}

	private AttributeValue getAttValue(com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue value) {

		Pattern p = Pattern.compile("\\{(\\w+): (\\w+),}");
		Matcher m = p.matcher(value.toString());
		if (m.find()) {
			if ("S".equals(m.group(1))){
				return AttributeValue.builder().s(String.valueOf(m.group(2))).build();
			}
			if ("N".equals(m.group(1))){
				return AttributeValue.builder().n(String.valueOf(m.group(2))).build();
			}
		}
        return null;
	}

	private void pushToDB(HashMap<String, AttributeValue> itemValues) {

		Region region = Region.EU_CENTRAL_1;
		DynamoDbClient ddb = DynamoDbClient.builder()
				.region(region)
				.build();

		String tableName = "cmtr-98b50f87-"+"Audit"+"-test";

		PutItemRequest request = PutItemRequest.builder()
				.tableName(tableName)
				.item(itemValues)
				.build();

		try {
			PutItemResponse response = ddb.putItem(request);
			System.out.println(tableName + " was successfully updated. The request id is "
					+ response.responseMetadata().requestId());

		} catch (ResourceNotFoundException e) {
			System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
			System.err.println("Be sure that it exists and that you've typed its name correctly!");
			System.exit(1);
		} catch (DynamoDbException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private AttributeValue getTime() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset

		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return AttributeValue.builder().s(nowAsISO).build();
	}

	private HashMap <String,String> dynamoJsonToMap(String dynamoJson){

		String[] arr = dynamoJson.split(",},");

		HashMap <String,String> res = new HashMap<>();

		for (String elm : arr){
			System.out.println(elm);
			Pattern p = Pattern.compile("\\{?(\\w+)\\=\\{\\w: (\\w+)");
			Matcher m = p.matcher(elm);
			if (m.find()) {
				System.out.println(m.group(1)+" : "+m.group(2));
				res.put(m.group(1),m.group(2));
			}
		}
		return res;
	}

	@Test
	public void test(){
			String s = "{value2={N: 3072,}, value={N: 2048,}, key={S: MaxMemory,}}";
		Map map = dynamoJsonToMap(s);
		assertEquals("3072",map.get("value2"));
		assertEquals("2048",map.get("value"));
		assertEquals("MaxMemory",map.get("key"));
	}

	@Test
	public void testGetAttValueString(){
		String elm = "{S: CACHE_TTL_SEC,}";
		com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue att = new com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue();
		att.setS("{S: CACHE_TTL_SEC,}");
		AttributeValue a = getAttValue(att);

		assertEquals("CACHE_TTL_SEC",a.s());
		assertEquals(AttributeValue.Type.S,a.type());

	}

	@Test
	public void testGetAttValueNumber(){
		String elm = "{N: 3600,}";
		com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue att = new com.amazonaws.services.lambda.runtime.events.models.dynamodb.AttributeValue();
		att.setN(elm);
		AttributeValue a = getAttValue(att);

		assertEquals("3600",a.n());
		assertEquals(AttributeValue.Type.N,a.type());

	}


}
