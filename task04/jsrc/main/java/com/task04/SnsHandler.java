package com.task04;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.syndicate.deployment.annotations.events.SnsEventSource;
import com.syndicate.deployment.annotations.events.SqsTriggerEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.RetentionSetting;

import java.util.*;

@LambdaHandler(lambdaName = "sns_handler",
	roleName = "sns_handler-role",
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)

@SnsEventSource(targetTopic = "lambda_topic")

public class SnsHandler implements RequestHandler<SNSEvent, Boolean> {

	LambdaLogger logger;

	public Boolean handleRequest(SNSEvent event, Context context) {
		logger = context.getLogger();
		List<SNSEvent.SNSRecord> records = event.getRecords();
		if (!records.isEmpty()) {
			Iterator<SNSEvent.SNSRecord> recordsIter = records.iterator();
			while (recordsIter.hasNext()) {
				logger.log(String.valueOf(recordsIter.next().getSNS()));
			}
		}
		return Boolean.TRUE;
	}
}
