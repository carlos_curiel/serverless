package com.task07;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import com.syndicate.deployment.annotations.environment.EnvironmentVariables;
import com.syndicate.deployment.annotations.events.EventBridgeRuleSource;
import com.syndicate.deployment.annotations.events.RuleEventSource;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.RetentionSetting;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

@LambdaHandler(lambdaName = "uuid_generator",
	roleName = "uuid_generator-role",
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)

@RuleEventSource(targetRule = "uuid_trigger")

@EnvironmentVariables(value = {
		@EnvironmentVariable(key = "bucket", value = "${target_bucket}")
})


public class UuidGenerator implements RequestHandler<Object, Map<String, Object>> {

	public Map<String, Object> handleRequest(Object request, Context context) {


		String filename = getTime();

		S3Client s3Client = S3Client.builder().build();
/**
 * @param bucketName
 *            The name of the bucket to place the new object in.
 * @param key
 *            The key of the object to create.
 * @param content
 *            The String to encode
 */

		ArrayList<String> al = new ArrayList<>();
		for (int i=0;i<10;i++){
			al.add(String.valueOf(UUID.randomUUID()));
		}

		HashMap<String,List<String>> hm = new HashMap<>();
		hm.put("ids",al);

		Gson gson = new Gson();
		String contents = gson.toJson(hm);

		String bucketName = System.getenv("bucket");

		PutObjectRequest req = PutObjectRequest.builder().key(filename).bucket(bucketName).build();
		s3Client.putObject(req, RequestBody.fromString(String.valueOf(contents)));


		System.out.println("Hello from lambda");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("statusCode", 200);
		resultMap.put("body", "Hello from Lambda");
		return resultMap;
	}

	private String getTime() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset

		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}


}
