package com.task08;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.annotations.lambda.LambdaLayer;
import com.syndicate.deployment.annotations.lambda.LambdaUrlConfig;
import com.syndicate.deployment.model.Architecture;
import com.syndicate.deployment.model.ArtifactExtension;
import com.syndicate.deployment.model.DeploymentRuntime;
import com.syndicate.deployment.model.RetentionSetting;
import com.syndicate.deployment.model.lambda.url.AuthType;
import com.syndicate.deployment.model.lambda.url.InvokeMode;

import coresearch.cvurl.io.model.Response;
import coresearch.cvurl.io.request.CVurl;
import org.testng.annotations.Test;


@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role",
	layers = {"sdk-layer"},
	runtime = DeploymentRuntime.JAVA21,
	isPublishVersion = false,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)
@LambdaLayer(
		layerName = "sdk-layer",
		libraries = {"lib/cvurl-io-1.5.1.jar"},
		runtime = DeploymentRuntime.JAVA21,
		architectures = {Architecture.X86_64},
		artifactExtension = ArtifactExtension.ZIP
)
@LambdaUrlConfig(
		authType = AuthType.NONE,
		invokeMode = InvokeMode.BUFFERED
)
public class ApiHandler implements RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {

	public APIGatewayV2HTTPResponse  handleRequest(APIGatewayV2HTTPEvent requestEvent, Context context) {
		System.out.println("Hello from lambda");

		APIGatewayV2HTTPResponse response = null;

		var path = requestEvent.getRequestContext().getHttp().getPath();
		if ("/weather".equals(path)) {

			String res = String.valueOf(getForecast());

			response = new APIGatewayV2HTTPResponse();
			response.setBody(res);
			response.setStatusCode(200);
		} else {
			return APIGatewayV2HTTPResponse.builder()
					.withStatusCode(400)
					.withBody("{\"No data\"}").build();
		}

		return response;
	}


		public String getForecast() {


			CVurl cVurl = new CVurl();

			//GET
			Response<String> response = cVurl.get("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m")
					.asString()
					.orElseThrow(RuntimeException::new);

			return response.getBody();

		}

		@Test
	    public void test(){
			System.out.println(getForecast());
		}



}
