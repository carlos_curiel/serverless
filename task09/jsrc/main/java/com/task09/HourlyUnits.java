package com.task09;

public class HourlyUnits{
    public String temperature_2m;
    public String time;

    @Override
    public String toString() {
        return "HourlyUnits{" +
                "temperature_2m='" + temperature_2m + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
