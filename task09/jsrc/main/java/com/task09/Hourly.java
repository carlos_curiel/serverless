package com.task09;

import java.util.ArrayList;
import java.util.HashSet;

public class Hourly{
    @Override
    public String toString() {
        return "Hourly{" +
                "temperature_2m=" + temperature_2m +
                ", time=" + time +
                '}';
    }

    public HashSet<String> temperature_2m;
    public ArrayList<String> time;
}
