package com.task09;

public class Forecast{
    public double elevation;
    public double generationtime_ms;

    @Override
    public String toString() {
        return "Forecast{" +
                "elevation=" + elevation +
                ", generationtime_ms=" + generationtime_ms +
                ", hourly=" + hourly +
                ", hourly_units=" + hourly_units +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", timezone='" + timezone + '\'' +
                ", timezone_abbreviation='" + timezone_abbreviation + '\'' +
                ", utc_offset_seconds=" + utc_offset_seconds +
                '}';
    }

    public Hourly hourly;
    public HourlyUnits hourly_units;
    public double latitude;
    public double longitude;
    public String timezone;
    public String timezone_abbreviation;
    public int utc_offset_seconds;
}

