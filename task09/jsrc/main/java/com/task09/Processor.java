package com.task09;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2HTTPResponse;
import com.google.gson.Gson;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.annotations.lambda.LambdaUrlConfig;
import com.syndicate.deployment.model.RetentionSetting;
import com.syndicate.deployment.model.*;
import com.syndicate.deployment.model.lambda.url.AuthType;
import com.syndicate.deployment.model.lambda.url.InvokeMode;
import com.task09.request.RootReq;
import coresearch.cvurl.io.model.Response;
import coresearch.cvurl.io.request.CVurl;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Collectors;

@LambdaHandler(lambdaName = "processor",
	roleName = "processor-role",
	isPublishVersion = false,
	tracingMode = TracingMode.Active,
	logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)
@LambdaUrlConfig(
		authType = AuthType.NONE,
		invokeMode = InvokeMode.BUFFERED
)
public class Processor implements RequestHandler<APIGatewayV2HTTPEvent, APIGatewayV2HTTPResponse> {

	public APIGatewayV2HTTPResponse handleRequest(APIGatewayV2HTTPEvent requestEvent, Context context) {
		System.out.println("Hello from lambda");

		APIGatewayV2HTTPResponse response = null;

		var path = requestEvent.getRequestContext().getHttp().getPath();


			doStuff();

			response = new APIGatewayV2HTTPResponse();
			response.setBody("{\"All good\"}");
			response.setStatusCode(200);


		return response;
	}

	public String getForecast() {


		CVurl cVurl = new CVurl();

		//GET
		Response<String> response = cVurl.get("https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m")
				.asString()
				.orElseThrow(RuntimeException::new);

		return response.getBody();

	}

	public void doStuff(){
		Gson gson = new Gson();
		String json = getForecast();
		RootReq r = gson.fromJson(json, RootReq.class);

		System.out.println(r.toString());

		Forecast f = new Forecast();
		f.elevation = r.elevation;
		f.generationtime_ms = r.generationtime_ms;
		f.hourly = new Hourly();
		f.hourly.temperature_2m = r.hourly.temperature_2m;
		f.hourly.time = r.hourly.time;
		f.hourly_units = new HourlyUnits();
		f.hourly_units.temperature_2m = r.hourly_units.temperature_2m;
		f.hourly_units.time = r.hourly_units.time;
		f.latitude = r.latitude;
		f.longitude = r.longitude;
		f.timezone = r.timezone;
		f.timezone_abbreviation = r.timezone_abbreviation;
		f.utc_offset_seconds = r.utc_offset_seconds;

		System.out.println(f);

		UUID uuid = UUID.randomUUID();
		HashMap<String, AttributeValue> itemValues = new HashMap<>();
		itemValues.put("id", AttributeValue.builder().s(String.valueOf(uuid)).build());

		HashMap<String, AttributeValue> forecast = new HashMap<>();
		forecast.put("elevation",AttributeValue.builder().n(String.valueOf(f.elevation)).build());
		forecast.put("generationtime_ms",AttributeValue.builder().n(String.valueOf(f.generationtime_ms)).build());

		HashMap<String, AttributeValue> hourly = new HashMap<>();
		hourly.put("temperature_2m",AttributeValue.builder().ns(f.hourly.temperature_2m).build());
		//hourly.put("time",AttributeValue.builder().l(AttributeValue.fromS(f.hourly.time.get(0)),AttributeValue.fromS(f.hourly.time.get(1))).build());
		hourly.put("time",AttributeValue.builder().l(f.hourly.time.stream().map(s->AttributeValue.fromS(s)).collect(Collectors.toList())).build());
		forecast.put("hourly",AttributeValue.builder().m(hourly).build());

		HashMap<String, AttributeValue> hourlyUnits = new HashMap<>();
		hourlyUnits.put("temperature_2m",AttributeValue.builder().ss(String.valueOf(f.hourly_units.temperature_2m)).build());
		hourlyUnits.put("time",AttributeValue.builder().ss(String.valueOf(f.hourly_units.time)).build());
		forecast.put("hourly_units",AttributeValue.builder().m(hourlyUnits).build());


		forecast.put("latitude",AttributeValue.builder().n(String.valueOf(f.latitude)).build());
		forecast.put("longitude",AttributeValue.builder().n(String.valueOf(f.longitude)).build());
		forecast.put("timezone",AttributeValue.builder().s(String.valueOf(f.timezone)).build());
		forecast.put("timezone_abbreviation",AttributeValue.builder().s(String.valueOf(f.timezone_abbreviation)).build());
		forecast.put("utc_offset_seconds",AttributeValue.builder().n(String.valueOf(f.utc_offset_seconds)).build());

		itemValues.put("forecast", AttributeValue.builder().m(forecast).build());

		System.out.println(itemValues);

		Region region = Region.EU_CENTRAL_1;
		DynamoDbClient ddb = DynamoDbClient.builder()
				.region(region)
				.build();

		String tableName = "cmtr-98b50f87-Weather-test";

		try {
			System.out.println("size: "+itemValues.entrySet().size());

			PutItemRequest request = PutItemRequest.builder()
					.tableName(tableName)
					.item(itemValues)
					.build();

			PutItemResponse response = ddb.putItem(request);
			System.out.println(tableName + " was successfully updated. The request id is "
					+ response.responseMetadata().requestId());

		} catch (ResourceNotFoundException e) {
			System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
			System.err.println("Be sure that it exists and that you've typed its name correctly!");
			System.exit(1);
		} catch (DynamoDbException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}



	}
}
