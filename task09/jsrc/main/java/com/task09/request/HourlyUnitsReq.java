package com.task09.request;

public class HourlyUnitsReq{
    public String relative_humidity_2m;
    public String temperature_2m;
    public String time;
    public String wind_speed_10m;

    @Override
    public String toString() {
        return "HourlyUnitsReq{" +
                "relative_humidity_2m='" + relative_humidity_2m + '\'' +
                ", temperature_2m='" + temperature_2m + '\'' +
                ", time='" + time + '\'' +
                ", wind_speed_10m='" + wind_speed_10m + '\'' +
                '}';
    }
}
