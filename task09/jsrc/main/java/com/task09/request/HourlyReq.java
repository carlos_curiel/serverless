package com.task09.request;

import java.util.ArrayList;
import java.util.HashSet;

public class HourlyReq{
    public ArrayList<Integer> relative_humidity_2m;
    public HashSet<String> temperature_2m;

    @Override
    public String toString() {
        return "HourlyReq{" +
                "relative_humidity_2m=" + relative_humidity_2m +
                ", temperature_2m=" + temperature_2m +
                ", time=" + time +
                ", wind_speed_10m=" + wind_speed_10m +
                '}';
    }

    public ArrayList<String> time;
    public ArrayList<Double> wind_speed_10m;
}
