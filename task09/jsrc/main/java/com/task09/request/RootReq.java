package com.task09.request;

public class RootReq{
    public CurrentReq current;
    public CurrentUnitsReq current_units;
    public double elevation;
    public double generationtime_ms;
    public HourlyReq hourly;
    public HourlyUnitsReq hourly_units;
    public double latitude;
    public double longitude;
    public String timezone;
    public String timezone_abbreviation;
    public int utc_offset_seconds;

    @Override
    public String toString() {
        return "RootReq{" +
                "current=" + current +
                ", current_units=" + current_units +
                ", elevation=" + elevation +
                ", generationtime_ms=" + generationtime_ms +
                ", hourly=" + hourly +
                ", hourly_units=" + hourly_units +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", timezone='" + timezone + '\'' +
                ", timezone_abbreviation='" + timezone_abbreviation + '\'' +
                ", utc_offset_seconds=" + utc_offset_seconds +
                '}';
    }
}
