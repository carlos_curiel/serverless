package com.task09.request;

public class CurrentReq{
    public int interval;
    public double temperature_2m;
    public String time;

    @Override
    public String toString() {
        return "CurrentReq{" +
                "interval=" + interval +
                ", temperature_2m=" + temperature_2m +
                ", time='" + time + '\'' +
                ", wind_speed_10m=" + wind_speed_10m +
                '}';
    }

    public double wind_speed_10m;
}

