package com.task09.request;

public class CurrentUnitsReq{
    public String interval;
    public String temperature_2m;
    public String time;

    @Override
    public String toString() {
        return "CurrentUnitsReq{" +
                "interval='" + interval + '\'' +
                ", temperature_2m='" + temperature_2m + '\'' +
                ", time='" + time + '\'' +
                ", wind_speed_10m='" + wind_speed_10m + '\'' +
                '}';
    }

    public String wind_speed_10m;
}
